require 'spec_helper'
require File.dirname(__FILE__) + '/../app/portfolio'

describe Portfolio do
  let(:assets)    { { 'AAPL' => 0.5, 'FB' => 0.5 } }
  let(:portfolio) { Portfolio.new(assets)          }

  xit 'calculates last month performance' do
    pending 'This tests fails because it retrieves real prices from the API'
    performance = portfolio.last_month_performance

    expect(performance).to eq('11.57%')
  end
end
