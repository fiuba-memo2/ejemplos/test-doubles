require 'spec_helper'
require 'vcr_setup'

require File.dirname(__FILE__) + '/../app/portfolio'

describe Portfolio do
  let(:assets)    { { 'AAPL' => 0.5, 'FB' => 0.5 } }
  let(:portfolio) { Portfolio.new(assets)          }

  it 'calculates last month performance' do
    VCR.use_cassette('stock_api') do
      performance = portfolio.last_month_performance

      expect(performance).to eq('6.67%')
    end
  end
end
