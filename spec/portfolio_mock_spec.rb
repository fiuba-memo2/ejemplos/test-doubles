require 'spec_helper'
require File.dirname(__FILE__) + '/../app/portfolio'

describe Portfolio do
  let(:stock_api) { instance_double('StockApi') }

  let(:aapl_stock_prices) do
    [{ Portfolio::CLOSE_ATTRIBUTE => 10.0 },
     { Portfolio::CLOSE_ATTRIBUTE => 11.0 }]
  end

  let(:aapl_stock_chart) do
    instance_double('StockChart',
                    chart: aapl_stock_prices)
  end
  let(:fb_stock_prices) do
    [{ Portfolio::CLOSE_ATTRIBUTE => 5.0 },
     { Portfolio::CLOSE_ATTRIBUTE => 6.0 }]
  end

  let(:fb_stock_chart) do
    instance_double('StockChart',
                    chart: fb_stock_prices)
  end

  let(:assets) { { 'AAPL' => 0.5, 'FB' => 0.5 } }
  let(:portfolio) { Portfolio.new(assets, stock_api) }

  it 'calculates last month performance' do
    expect(stock_api).to receive(:chart)
      .with('AAPL', Portfolio::STOCK_RANGE) { aapl_stock_chart }

    expect(stock_api).to receive(:chart)
      .with('FB', Portfolio::STOCK_RANGE) { fb_stock_chart }

    performance = portfolio.last_month_performance

    expect(performance).to eq('15.0%')
  end
end
