require 'spec_helper'
require File.dirname(__FILE__) + '/../app/portfolio'

describe Portfolio do
  let(:stock_api) { instance_spy('StockApi') }
  let(:stock_prices) do
    [{ Portfolio::CLOSE_ATTRIBUTE => 10.0 },
     { Portfolio::CLOSE_ATTRIBUTE => 11.0 }]
  end
  let(:stock_chart) { double('StockChart', chart: stock_prices) }

  let(:assets)    { { 'AAPL' => 0.5, 'FB' => 0.5 }   }
  let(:portfolio) { Portfolio.new(assets, stock_api) }

  it 'calculates last month performance' do
    allow(stock_api).to receive(:chart) { stock_chart }

    performance = portfolio.last_month_performance

    expect(stock_api).to have_received(:chart).exactly(2).times

    expect(performance).to eq('10.0%')
  end
end
