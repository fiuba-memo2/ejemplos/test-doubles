require 'stock_quote'

##
# Calculates a portfolio performance
##
class Portfolio
  STOCK_RANGE = '1m'.freeze
  CLOSE_ATTRIBUTE = 'close'.freeze
  DECIMALS = 2

  def initialize(assets, stock_api = StockQuote::Stock)
    @assets = assets
    @stock_api = stock_api
  end

  def last_month_performance
    weighted_performances = @assets.map do |symbol, allocation|
      stock_quotes = fetch_stock_quotes(symbol)
      performance(stock_quotes.chart) * allocation
    end
    to_percentage weighted_performances.reduce(&:+)
  end

  private

  def fetch_stock_quotes(symbol)
    @stock_api.chart(symbol, STOCK_RANGE)
  end

  def performance(stock_chart)
    (stock_close(stock_chart.last) / stock_close(stock_chart.first)) - 1
  end

  def stock_close(quote)
    quote[CLOSE_ATTRIBUTE]
  end

  def to_percentage(a_num)
    "#{(a_num * 100).round(DECIMALS)}%"
  end
end
